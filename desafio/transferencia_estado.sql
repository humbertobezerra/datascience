-- Table: tcem.transferencia_estado

-- DROP TABLE tcem.transferencia_estado;

CREATE TABLE tcem.transferencia_estado
(
  sig_uf character(2) NOT NULL, -- Sigla do estado para o qual a transferência foi realizada
  ano_referencia integer NOT NULL, -- Ano de referência que ocorreu a transferência
  mes_referencia smallint NOT NULL, -- Mês de referência que ocorreu a transferência
  vlr_decendio1 numeric(15,2) NOT NULL, -- Volume financeiro transferido no 1o decêncio do mês de referência
  vlr_decendio2 numeric(15,2) NOT NULL, -- Volume financeiro transferido no 2o decêncio do mês de referência
  vlr_decendio3 numeric(15,2) NOT NULL, -- Volume financeiro transferido no 3o decêncio do mês de referência
  des_item character varying(30) NOT NULL, -- Indica qual o item da transferência (ex: IPI-EXP na transferência FUNDEB)
  des_transferencia character varying(50), -- Indica qual a transferência (ex: FPM)
  des_vazio character(1)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tcem.transferencia_estado
  OWNER TO siafi;
GRANT ALL ON TABLE tcem.transferencia_estado TO siafi;
GRANT SELECT ON TABLE tcem.transferencia_estado TO desafio_inovacao;
COMMENT ON TABLE tcem.transferencia_estado
  IS 'Transferências Constitucionais para Estados (Fonte: http://www.tesourotransparente.gov.br/ckan/dataset/transferencias-constitucionais-para-estados)';
COMMENT ON COLUMN tcem.transferencia_estado.sig_uf IS 'Sigla do estado para o qual a transferência foi realizada';
COMMENT ON COLUMN tcem.transferencia_estado.ano_referencia IS 'Ano de referência que ocorreu a transferência';
COMMENT ON COLUMN tcem.transferencia_estado.mes_referencia IS 'Mês de referência que ocorreu a transferência';
COMMENT ON COLUMN tcem.transferencia_estado.vlr_decendio1 IS 'Volume financeiro transferido no 1o decêncio do mês de referência';
COMMENT ON COLUMN tcem.transferencia_estado.vlr_decendio2 IS 'Volume financeiro transferido no 2o decêncio do mês de referência';
COMMENT ON COLUMN tcem.transferencia_estado.vlr_decendio3 IS 'Volume financeiro transferido no 3o decêncio do mês de referência';
COMMENT ON COLUMN tcem.transferencia_estado.des_item IS 'Indica qual o item da transferência (ex: IPI-EXP na transferência FUNDEB)';
COMMENT ON COLUMN tcem.transferencia_estado.des_transferencia IS 'Indica qual a transferência (ex: FPM)';


-- Index: tcem.transferencia_estado_ano_referencia_mes_referencia_sig_uf_d_idx

-- DROP INDEX tcem.transferencia_estado_ano_referencia_mes_referencia_sig_uf_d_idx;

CREATE INDEX transferencia_estado_ano_referencia_mes_referencia_sig_uf_d_idx
  ON tcem.transferencia_estado
  USING btree
  (ano_referencia, mes_referencia, sig_uf COLLATE pg_catalog."default", des_item COLLATE pg_catalog."default");

-- Index: tcem.transferencia_estado_des_item_ano_referencia_mes_referencia_idx

-- DROP INDEX tcem.transferencia_estado_des_item_ano_referencia_mes_referencia_idx;

CREATE INDEX transferencia_estado_des_item_ano_referencia_mes_referencia_idx
  ON tcem.transferencia_estado
  USING btree
  (des_item COLLATE pg_catalog."default", ano_referencia, mes_referencia, sig_uf COLLATE pg_catalog."default", des_item COLLATE pg_catalog."default");

-- Index: tcem.transferencia_estado_des_item_sig_uf_des_item1_ano_referenc_idx

-- DROP INDEX tcem.transferencia_estado_des_item_sig_uf_des_item1_ano_referenc_idx;

CREATE INDEX transferencia_estado_des_item_sig_uf_des_item1_ano_referenc_idx
  ON tcem.transferencia_estado
  USING btree
  (des_item COLLATE pg_catalog."default", sig_uf COLLATE pg_catalog."default", des_item COLLATE pg_catalog."default", ano_referencia, mes_referencia);

-- Index: tcem.transferencia_estado_sig_uf_ano_referencia_mes_referencia_d_idx

-- DROP INDEX tcem.transferencia_estado_sig_uf_ano_referencia_mes_referencia_d_idx;

CREATE INDEX transferencia_estado_sig_uf_ano_referencia_mes_referencia_d_idx
  ON tcem.transferencia_estado
  USING btree
  (sig_uf COLLATE pg_catalog."default", ano_referencia, mes_referencia, des_item COLLATE pg_catalog."default");

